import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '@/pages/Home.vue'
import Tables from '@/pages/Tables.vue'
import Databases from '@/pages/Databases.vue'
import Users from '@/pages/Users.vue'
import Roles from '@/pages/Roles.vue'
import Settings from '@/pages/Settings.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },

  {
    path: '/tables',
    name: 'tables',
    component: Tables,
    children: [
      { path: '/tables/:list_name', component: Tables }
    ],
  },

  {
    path: '/databases',
    name: 'databases',
    component: Databases,
    children: [
        { path: '/databases/:list_name', component: Databases,}
    ]
  },

  {
    path: '/users',
    name: 'users',
    component: Users,
    children: [
      { path: '/users/:list_name', component: Users }
    ]
  },

  {
    path: '/roles',
    name: 'roles',
    component: Roles,
    children: [
      { path: '/roles/:list_name', component: Roles }
    ]
  },

  {
    path: '/settings',
    name: 'settings',
    component: Settings
  },

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
