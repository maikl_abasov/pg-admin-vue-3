
import FormEditor from "@/components/form/FormEditor";
import FormSubmit from "@/components/form/FormSubmit";
import FormContainer from "@/components/form/FormContainer";
import UniversalForm from "@/components/UniversalForm";

import FormInput from "@/components/form/FormInput";
import FormSelect from "@/components/form/FormSelect";
import FormTextarea from "@/components/form/FormTextarea";
import FormCheckbox from "@/components/form/FormCheckbox";
import FormRadio from "@/components/form/FormRadio";

import SectionPanel from "@/components/SectionPanel";
import TableRender from "@/components/TableRender";

const GlobalMixin = {
    install(Vue) {

        Vue.component('FormEditor', FormEditor);
        Vue.component('FormContainer', FormContainer);
        Vue.component('FormSubmit', FormSubmit);
        Vue.component('UniForm', UniversalForm);

        Vue.component('FormInput', FormInput);
        Vue.component('FormSelect', FormSelect);
        Vue.component('FormTextarea', FormTextarea);
        Vue.component('FormCheckbox', FormCheckbox);
        Vue.component('FormRadio', FormRadio);

        Vue.component('VSection', SectionPanel);
        Vue.component('TableRender', TableRender);

        Vue.mixin({
            data() {return {}},
            computed: {},
            methods: {
                showModal (prop) {
                    this[prop] = false;
                },
                closeModal (prop) {
                    this[prop] = false;
                },
                setTimer(fn, timer = 3000) {
                    setTimeout(fn, timer)
                },
                setToken(token) {
                    const tokenName = 'X-JWT-TOKEN';
                    localStorage.setItem(tokenName, token)
                },
                getToken() {
                    const tokenName = 'X-JWT-TOKEN';
                    return localStorage.getItem(tokenName)
                },
                deleteToken() {
                    const tokenName = 'X-JWT-TOKEN';
                    localStorage.removeItem(tokenName)
                },
                store(key, value = null) {
                    if (!value) return localStorage.getItem(key)
                    localStorage.setItem(key, value)
                },
                storeRemove(key) {
                    localStorage.removeItem(key)
                },
            } // --- Methods
        }) // --- Mixin
    }
}

export default GlobalMixin
