import axios from 'axios'

// const API_URL = 'http://db-control/pg-admin-interface-new/api/api.php';
const API_URL = apiUrl;

axios.defaults.baseURL = API_URL;

// Различаем адрес интерфейса по умолчанию в соответствии с переменными среды
// switch (process.env.NODE_ENV) {
//     case "production":
//         axios.defaults.baseURL = "http://127.0.0.1:3000";
//         break;
//     case "test":
//         axios.defaults.baseURL = "http://192.168.1.1";
//         break;
//     default:
//         axios.defaults.baseURL = API_URL;
// }

// axios.defaults.withCredentials = true; // Установите CORS, чтобы разрешить междоменные учетные данные

axios.defaults.timeout = 10000; // Устанавливаем период ожидания
axios.defaults.headers['Content-Type'] = 'application/json'

// Настраиваем перехватчик запросов
axios.interceptors.request.use((config) => {
    // let token = localStorage.getItem('token');
    // if(token) config.headers.Authorization = token;
    return config
}, error => {
    return Promise.reject(error)
})

// Ответный перехватчик
axios.interceptors.response.use(response => {
    return response.data;
}, error => {

    let {response} = error;
    if (response) {
        // Сервер возвращает результат не менее
        switch (response.status) {
            case 401:
                console.log('HTTP-ERROR: 401')
                console.log('Проблема с разрешением, текущий запрос требует проверки пользователя, обычно он не вошел в систему')
                break;

            case 403:
                localStorage.removeItem('token')
                console.log('HTTP-ERROR: 403')
                console.log('Сервер понял запрос, но отказывается его выполнить, обычно из-за истечения срока действия токена или сеанса')
                break;

            case 404:
                console.log('HTTP-ERROR: 404')
                console.log('Страница не найдена')
                break;

            default:
                console.log('HTTP-ERROR: Не определен статус ответа')
                console.log('response.status:' + response.status)
                break;
        }

    } else {

        console.log('HTTP-ERROR: Сервер даже не вернул результат')
        if (!window.navigator.onLine) {
            console.log('Если клиент отключен: вы можете перейти на отключенную страницу')
            return
        }

        console.log('Это может быть ошибка сервера, возвращающая обещание')
        console.log('ERROR: ' + error)
        return Promise.reject(error)
    }

});

export default axios