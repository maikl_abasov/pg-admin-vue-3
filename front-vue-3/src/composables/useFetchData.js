import {reactive, ref, onBeforeMount } from 'vue';
import {useRouter, useRoute} from 'vue-router'

import ApiClient from '@/plugins/api.client';

const useState  = reactive({
    tables: [],
    users: [],
    roles: [],
    databases: [],
    fields: {},

    tableName: '',
    fieldName: '',
    dbName: '',
    userName: '',
    roleName: '',
});

//const tokenName = ref('jwt_token');

export default function useFetchData() {

    const router = useRouter()
    const route = useRoute()

    const redirect = (url) => {
        router.push({ path: url })
    }

    const getUrlParam = (paramName) => {
        let itemName = route.params[paramName];
        return (itemName) ? itemName : null;
    }

    const isLeftMenuActive = (name, param) => {
        let item = {};
        let prop  = (param.prop) ? param.prop : 'tables';
        let fName = (param.fname) ? param.fname : 'table_name';
        for(let i in useState[prop]) {
            useState[prop][i]['is_active'] = false;
            let curName = useState[prop][i][fName];
            if(curName !== name)
                continue;
            useState[prop][i]['is_active'] = true;
            item = useState[prop][i];
        }
        return item;
    }

    const setPropState = (prop, data) => {
        useState[prop] = data;
    }

    const getPropState = (prop) => {
        return useState[prop];
    }

    const send = async (url, prop, callback = null) => {
        const response = await ApiClient.get(url);
        const data = response.data;
        setPropState(prop, data);
        if(callback) return callback(data);

        return new Promise((resolve, reject) => {
              resolve(data);
        });
    }

    const getTables = (callback = null) => {
        let url = '/GET_TABLE_LIST';
        return send(url, 'tables', callback)
    }

    const getDatabases = (callback = null) => {
        let url = '/SHOW_DATABASE_LIST';
        return send(url, 'databases', callback)
    }

    const getUsers = (callback = null) => {
        let url = '/getDbUsersList';
        return send(url, 'users', callback)
    }

    const getFields = (tableName, callback = null) => {
        let url = '/GET_TABLE_FIELDS/' + tableName;
        setPropState('tableName', tableName)
        return send(url, 'fields', callback);
    }

    return {
        useState,
        getTables,
        getFields,
        getDatabases,
        getUsers,
        getPropState,
        setPropState,
        isLeftMenuActive,
        redirect,
        getUrlParam,
    }
}