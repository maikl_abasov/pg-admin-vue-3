import ApiClient from "@/plugins/api.client";

export default function useTableAction() {

    const sendGet = async (url, callback = null) => {
        let response = await ApiClient.get(url);
        if(callback) callback(response)
        return response
    }

    const sendPost = async (url, data, callback = null) => {
        let response = await ApiClient.post(url, data);
        if(callback) callback(response)
        return response
    }

    const send = async (url, callback = null, method = 'get') => {
        const response = await ApiClient[method](url);
        const data = response.data;
        if(callback) return callback(data);
        return new Promise((resolve, reject) => {
            resolve(data);
        });
    }

    //-------------------------------
    // Функции для работы с индексами и ключами таблицы


    //-------------------------------
    // Функции для работы с полями таблицы
    const createField = (data, callback = null) => {
        let tableName = data.table_name;
        let name = data.name;
        let type = data.type;
        let size = data.size;

        const url = `/ADD_FIELD/${tableName}/${name}/${type}`;
        return send(url, callback, 'get')
    }

    const deleteField = (data, callback = null) => {
        let tableName = data.table_name;
        let name = data.name;
        const url = `/DELETE_FIELD/${tableName}/${name}`;
        return send(url, callback, 'get')
    }

    const renameField = (data, callback = null) => {
        let tabName = data.tableName
        let newName = data.newName
        let oldName = data.oldName

        if (oldName == newName || !oldName || !newName)
            return false;

        const url = `/RENAME_FIELD/${tabName}/${oldName}/${newName}`;
        return send(url, callback, 'get')
    }

    //-------------------------------
    // Функции для работы с таблицами
    const renameTable = (oldName, newName, callback = null) => {
        if ( !oldName || !newName || (oldName == newName))
            return false;

        const url = '/RENAME_TABLE/' + oldName + '/' + newName;
        return send(url, callback, 'get')
    }

    const createTable = (data, callback = null) => {
        let name = data.name;
        let id   = data.id;
        const url = `/CREATE_TABLE/${name}/${id}`;
        return send(url, callback, 'get')
    }

    const copyTable = (tabName, newTabName, callback = null) => {
        if ( !tabName || !newTabName || (tabName == newTabName))
            return false;

        const url = '/COPY_TABLE/' + tabName + '/' + newTabName;
        return send(url, callback, 'get')
    }

    const deleteTable = (tableName, callback = null) => {
        const url = '/DELETE_TABLE/' + tableName
        return send(url, callback, 'get')
    }

    //--- RETURN VALUES
    return {
        createTable,
        deleteTable,
        copyTable,
        renameTable,
        //---------
        renameField,
        createField,
        deleteField,
    }
}
