import {reactive, ref } from 'vue';

const alertService  = reactive({
    status: false,
    type: 'info',
    message: 'Успешное сохранение',
    timer: 1000,
});

const lastActionMessage = ref('');

export default function useAlertMessages() {

    const setMessage = (message, status = true, type = 'info', timer =true) => {

        lastActionMessage.value = message;

        alertService.message = message;
        alertService.type    = type;
        alertService.status  = status;

        if(timer) {
            setTimeout(() => {
                alertService.status  = false;
            }, alertService.timer)
        }
    }

    const getMessage = () => {
        return alertService;
    }

    return {
        alertService,
        setMessage,
        getMessage,
        lastActionMessage,
    }
}