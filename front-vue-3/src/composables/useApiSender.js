import ApiClient from "@/plugins/api.client";
import useFetchData from "@/composables/useFetchData"

export default function useActionSender() {

    const fetchData = useFetchData();
    const useState  = fetchData.useState;

    const send = async (url, callback = null, data = null, method = 'get') => {
        const response = await ApiClient[method](url);
        const result = response.data;
        if(callback) return callback(result);
        return new Promise((resolve, reject) => {
            resolve(result);
        });
    }

    const setParam = (url, param = null) => {
        if(!param) return url;
        for(let i in param) {
            let value = param[i];
            if(value) url = url + '/' + value
        }
        return url;
    }

    const alertMessage = (message = '') => {
        alert(message);
    }

    // ------------------------
    // ---- sendAction --------
    const sendAction = (action, param = null, callback = null, method = 'get') => {

        //$tableName = $this->isEmpty($params, 0);
        //$fieldName = $this->isEmpty($params, 1);

        let apiUrl = null;
        let postData = null;
        let url = '/' + action;

        switch (action) {

            case 'CHANGE_USER_PASSWORD' :
                // $userName     = $this->isEmpty($params, 0);
                // $newPassword  = $this->isEmpty($params, 1);
                // $res = $this->changeUserPassword($userName, $newPassword);

                break;

            case 'SAVE_CONFIG' :
                // $newConfig = $this->getPostData();
                // $res = saveConfig($newConfig, $this->config, CONF_FILE_NAME, CONF_DIR);

                break;

            case 'ADD_FIELD_LIST' :
                break;

            case 'GET_CUR_CONFIG' :
                // $res = $this->config;
                apiUrl = '/' + action;
                break;

            case 'GET_FILE_USERS_CONFIG' :
                // $res = getFileUsersConfig('config/dbUsers/');

                break;

            case 'CURRENT_DATABASE' :
                // $res = $this->currentDatabase();
                apiUrl = '/' + action;
                break;

            case 'EXEC_SQL_COMMAND' :
                // $command     = $this->isEmpty($params, 0);
                // $commandType = $this->isEmpty($params, 1);
                // $tableName   = $this->isEmpty($params, 2);
                // $res = $this->execSqlCommand($command, $commandType, $tableName);

                break;

            case 'CURRENT_DB_USER' :
                // $res = $this->getCurrentDbUser();
                apiUrl = url;
                break;

            case 'CREATE_DATABASE' :
                // /$newDbName/$ownerName
                // $this->createDatabase($newDbName, $ownerName);
                apiUrl =  setParam(url, param);
                break;

            case 'COPY_DATABASE' :
                // $dbName = $this->isEmpty($params, 0);
                // $newDbName = $this->isEmpty($params, 1);
                // $res = $this->copyDatabase($dbName, $newDbName);
                apiUrl =  setParam(url, param);
                break;

            case 'deleteDb' :
                // $dbName  = $this->isEmpty($params, 0);
                apiUrl =  setParam(url, param);
                break;


            case 'SHOW_DATABASE_LIST' :
                // $res = $this->showDatabaseList();
                apiUrl = url;
                break;

            case 'DELETE_ITEM' :
                // $fieldValue = $this->isEmpty($params, 2);
                // $res = $this->deleteItemRecord($tableName, $fieldName, $fieldValue);

                break;

            case 'GET_TABLE_LIST' :
                // $shemeName = $this->isEmpty($params, 0);
                // $tableList    = $this->getTableList($shemeName);
                // $res = $this->assocFormatted($tableList, 'table_name');
                apiUrl =  setParam(url, param);
                break;

            case 'GET_TABLE_FIELDS' :
                // $fieldsList = $this->getTableFields($tableName);
                // $res = $fieldsList;
                apiUrl =  setParam(url, param);
                break;

            case 'GET_TABLE_LIST_SHEME' :
                // $shemeName = $this->isEmpty($params, 0);
                // $res = $this->getTableListSheme($shemeName);

                break;

            case 'GET_TABLE_ID_NAME' :
                // $fieldList = $this->getTableFields($tableName);
                // $res = $this->getAutoIncrementName($fieldList);

                break;

            case 'GET_TABLE_DATA' :
                // $res = $this->getTableData($tableName);

                break;

            case 'CREATE_TABLE' :
                // /$tableName/$idName
                // $res['save_result'] = $this->createTable($tableName, $fieldName);

                break;

            case 'COPY_TABLE' :
                // $newTableName = $this->isEmpty($params, 1);
                // $withData = $this->isEmpty($params, 2);
                // $res['save_result'] = $this->copyTable($tableName, $newTableName, $withData);

                break;

            case 'CREATE_INDEX' :
                // $fieldName = $this->isEmpty($params, 1);
                // $res['save_result'] = $this->createIndex($tableName, $fieldName);

                break;

            case 'DROP_INDEX' :
                // $fieldName = $this->isEmpty($params, 1);
                // $res['save_result'] = $this->dropIndex($tableName, $fieldName);

                break;

            case 'TRUNCATE_TABLE' :
                // $res['save_result'] = $this->truncateTable($tableName);

                break;

            case 'DELETE_TABLE' :
                // $res['save_result'] = $this->deleteTable($tableName);

                break;

            case 'RENAME_TABLE' :
                // $newTableName = $this->isEmpty($params, 1);
                // $res = $this->renameTable($tableName, $newTableName);

                break;

            case 'ADD_FIELD' :
                // /$tableName/$fieldName/$fieldType/$fieldSize
                // $fieldType = $this->isEmpty($params, 2, $fieldType);
                // $res['save_result'] = $this->addField($tableName, $fieldName , $fieldType, $fieldSize);

                break;

            case 'DELETE_FIELD' :
                // $res['save_result'] = $this->deleteField($tableName, $fieldName);
                break;

            case 'RENAME_FIELD' :
                // $newFieldName = $this->isEmpty($params, 2);
                // $res['save_result'] = $this->renameField($tableName, $fieldName, $newFieldName);

                break;

            case 'EDIT_ITEM' :
                // $itemId = $this->isEmpty($params, 2);
                // $newValue = $this->isEmpty($params, 3);
                // $res = $this->editItem($tableName, $fieldName, $itemId, $newValue);

                break;

            case 'ADD_ITEM' :
                // $this->getPostData();
                // $res = $this->addItem($tableName, $this->postData);

                break;

            // Функции для работы с пользователем
            case 'ADD_DB_USER' :
                // $userName  = $this->isEmpty($params, 0);
                // $password  = $this->isEmpty($params, 1);
                // $dbName    = $this->isEmpty($params, 2);
                // $userState = $this->isEmpty($params, 3);
                // $res = $this->createUser($userName, $password, $dbName, $userState);

                break;

            case 'SET_USER_PRIVILEGES' :
                // $userName  = $this->isEmpty($params, 0);
                // $dbName    = $this->isEmpty($params, 1);
                // $res = $this->setUserPrivileges($userName, $dbName);

                break;

            case 'SET_SUPER_USER' :
                // $userName  = $this->isEmpty($params, 0);
                // $res = $this->setSuperUser($userName);

                break;

            case 'DELETE_DB_USER' :
                // $userName  = $this->isEmpty($params, 0);
                break;

            case 'createUser' :
                // let url = '/createUser/' + name + '/' + password + '/' + dbname + '/' + superState
                //param.push('dbtest1');
                //param.push('super');
                apiUrl =  setParam(url, param);
                break;

            case 'deleteDbUser' :
                // var url = 'deleteDbUser/' + name
                apiUrl =  setParam(url, param);
                break;

            case 'setUserPrivileges' :
                // const url = 'setUserPrivileges/' + userName + '/' + dbName
                apiUrl =  setParam(url, param);
                break;

            case 'delUserPrivileges' :
                // const url = 'delUserPrivileges/' + userName + '/' + dbName
                apiUrl =  setParam(url, param);
                break;

            // ---------- НОВЫЕ ЗАПРОСЫ --------------
            // setUserPrivileges/w1user/reestrsrv   установить привилегии к базе
            // setSuperUser/w1user  установить супер статус
            // let url = 'changeFieldType/' + tableName + '/' + fieldName + '/' + newType
            // var url = 'getDbUsersList'
            // let url = '/createUser/' + name + '/' + password + '/' + dbname + '/' + superState
            // var url = 'deleteDbUser/' + name
            // const url = 'setUserPrivileges/' + userName + '/' + dbName
            // const url = 'delUserPrivileges/' + userName + '/' + dbName
            // var url = 'setSuperUser/' + userName
            // var url = 'delSuperUser/' + userName
            // const url = 'addNewDb/' + this.newDbName
            // var url = 'deleteDb/' + name
            // var url = 'pg_dump.php?user=' + userName + '&password=' + userPassword + '&dbname=' + dbName

        }

        if(!apiUrl) {
            let errMessage = 'SEND-HTTP-ERROR: apiUrl=' + apiUrl + '; action=' + action;
            alertMessage(errMessage);
            console.log(errMessage);
            return false;
        }

        return send(apiUrl, callback, postData, method)
    }
    // ---- ./ sendAction -----
    // ------------------------


    const getDatabases = (callback = null) => {
        return sendAction('SHOW_DATABASE_LIST', null, callback);
    }

    const getTables = (callback = null, sheme = null) => {
        const param = (sheme) ? [sheme] : null;
        return sendAction('GET_TABLE_LIST', param, callback);
    }

    const getFields = (tableName, callback = null) => {
        return sendAction('GET_TABLE_FIELDS', [tableName], callback);
    }

    const getUsers = (callback = null) => {
        return send('/getDbUsersList', callback)
    }

    const setListActiveItem = (list, setFirstActive = false) => {
        let ch   = 1;
        let item = [];
        for(let i in list) {
            let elem = list[i];
            elem['is_active'] = false;
            if(setFirstActive && ch == 1) {
                elem['is_active'] = true;
                item = elem;
            }
            ch++;
        }
        return {
            list, item
        };
    }

    //-----  RETURN -----
    return {
        useState,
        fetchData,
        setListActiveItem,
        sendAction,
        getDatabases,
        getTables,
        getFields,
        getUsers,
    }
}
