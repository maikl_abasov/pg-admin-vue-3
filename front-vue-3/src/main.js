import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import globalPlugin from './plugins/global.plugin'

const Vm = createApp(App)
Vm.use(router)
Vm.use(globalPlugin)
Vm.config.errorHandler = errorShow;
Vm.mount('#app');

//////////////////////////
//////////////////////////
//////////////////////////

function errorShow(err) {
    console.log('==============START ERROR=================');
    console.log(err);
    console.log('===============END ERROR================');
}
