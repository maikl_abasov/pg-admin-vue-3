<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: text/html; charset=utf-8');
header('Content-Type: text/html; charset=utf-8');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

session_start();  // Запускаем сессию

define('ROOT_DIR', __DIR__);
define('CONF_DIR', ROOT_DIR . '/config');
define('CONF_FILE_NAME', CONF_DIR . '/conf.php');
define('CLASS_DIRECTORY', ROOT_DIR . '/src');

require_once ROOT_DIR . '/classAutoloader.php';

$pathInfo = getPathInfo();

$config     = require CONF_FILE_NAME;         // --- получаем конфиги
$dispatcher = new RequestDispatcher($config); // --- создаем объект
$response   = $dispatcher->route($pathInfo);  // --- выполняем
getResponse($response);                       // --- возвращаем результат

////////////////////////////////////////////////
////////////////////////////////////////////////
///////////////////////////////////////////////
//////////////////////////////////////////////

function getPathInfo($fName = 'PATH_INFO') {
    $server = $_SERVER;
    $routes = array();
    if(!empty($server[$fName])) {
        $pathInfo = $server[$fName];
        $pathInfo = trim($pathInfo, '/');
        $routes   = explode('/', $pathInfo);
    }
    return $routes;
}

function pre($data) {
    $r = print_r($data, true);
    echo '<pre>';
    echo print($r);
    echo '</pre>';
    die();
}

function getError($data) {
    getResponse($data, 'error');
}

function getDebug($data) {
    getResponse($data, 'debug');
}

function getResponse($data, $name = 'data') {
    die(json_encode(array($name => $data)));
}

function saveConfig($newData, $oldData, $fileName = 'config/conf.php', $confDir = 'config') {

    $newStringData = renderArrayToString($newData);
    $oldStringData = renderArrayToString($oldData);

    // сохраняем старый конфиг
    $oldFileName = $confDir . '/old_config/conf_' . date("Y_m_d___H_i_s") . '.txt';

    if(empty($oldData))  return false;

    file_put_contents($oldFileName, $oldStringData);

    if(empty($newData))  return false;

    // перезаписываем новый конфиг
    file_put_contents($fileName, $newStringData);

    return true;
}

// --- записывает данные пользователя в файл
function saveFileUserConfig($newData, $fileName = 'config/dbUsers/') {
    $userName = $newData['username'];
    $newData['create_date'] = date("Y_m_d___H_i_s");
    $fileName = $fileName . $userName. '.php';
    $newStringData = renderArrayToString($newData);
    file_put_contents($fileName, $newStringData);
    return true;
}

// --- получаем  данные пользователей из файлов
function getFileUsersConfig($dirName = 'config/dbUsers/') {

    $result = array();
    $files = scandir($dirName);

    foreach($files as $key => $name) {
        if($name == '.' || $name == '..') continue;
        $item = include $dirName . $name;
        $arrName = explode('.', $name);
        $userName = $arrName[0];
        $result[$userName] = $item;
    }

    // print_r($result); die;
    return $result;
}

// --- получаем дефолтные (базовые) настройки
function getDefaultConfig() {
    $config = include CONF_DIR .'/config_default.php';
    return $config;
}


function renderArrayToString($arrData) {
    $ch = 0;
    $stringData = "";
    foreach ($arrData as $key => $value) {
        ($ch) ? $sep = ',' :  $sep = '';
        $stringData .= "{$sep}'{$key}' => '{$value}' \n";
        $ch++;
    }
    $stringData = "<?php \n return array(\n {$stringData}); \n";
    return $stringData;
}


function s2($data) {

    $fileName = "my_file.php";
    $stringData = serialize($data);
    file_put_contents($fileName, $stringData);
    $stringData = file_get_contents($fileName);
    $configData = unserialize($stringData);
    return $configData;
}


function textareaHandler($text) {
    $result = explode("\n", $text);
    return $result;
}


function lg() {

    $debugTrace = debug_backtrace();
    $args = func_get_args();

    $get = false;
    $output = $traceStr = '';

    $style = 'margin:10px; padding:10px; border:3px red solid;';

    foreach ($args as $key => $value) {
        $itemArr = array();
        $itemStr = '';
        is_array($value) ? $itemArr = $value : $itemStr = $value;
        if ($itemStr == 'get') $get = true;
        $line = print_r($value, true);
        $output .= '<div style="' . $style . '" ><pre>' . $line . '</pre></div>';
    }

    foreach ($debugTrace as $key => $value) {
        // if($key == 'args') continue;
        $itemArr = array();
        $itemStr = '';
        is_array($value) ? $itemArr = $value : $itemStr = $value;
        if ($itemStr == 'get') $get = true;
        $line = print_r($value, true);
        $output .= '<div style="' . $style . '" ><pre>' . $line . '</pre></div>';
    }


    if ($get)  return $output;

    print $output;
    //print '<pre>' . print_r($debug) . '</pre>';
    die ;

}

?>
