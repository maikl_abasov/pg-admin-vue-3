<?php

trait DataHandlers {

    // -- Удаляем запись из базы
    protected function deleteItemRecord($tableName, $fieldName, $fieldValue) {
        $query = "
            DELETE FROM {$tableName} WHERE {$fieldName} = '{$fieldValue}';
        ";
        $result = $this -> exec($query);
        return $result;
    }

    protected function checkFieldType($tableName, $fieldName, $typeName = 'data_type') {
//        [create_at] => Array(
//            [column_name] => create_at
//            [column_default] =>
//            [data_type] => character varying
//        [auto_increment] =>
//            [input_type] => varchar
//        [form_type] => text )
        $fieldList = $this->getTableFields($tableName);
        // lg($fieldList);
        return $fieldList[$fieldName][$typeName];
    }

    // -- Редактируем запись в базы
    protected function editItem($tableName, $fieldName, $itemId, $newValue) {
        $fieldTypeName = $this->checkFieldType($tableName, $fieldName, 'data_type');
        $updateFieldData = "{$fieldName} = '{$newValue}'";
        switch($fieldTypeName) {
            case 'character varying' : break;
            case 'integer' :
                     $newValue = (int)$newValue;
                     if(!$newValue)
                         $newValue = "NULL";
                     $updateFieldData = "{$fieldName} = {$newValue}";
                     break;
            case 'text'    : break;
        }

        $idName = "id";
        $query  = "
            UPDATE {$tableName} SET {$updateFieldData} WHERE {$idName} = {$itemId}
        ";
        $result = $this ->exec($query);
        return $result;
    }

    // -- Добавляем новую запись в базу
    protected function addItem($tableName, $postData = array()) {
        $idName = "id";
        $fname = $fvalue = '';
        $fields = $this->getTableFields($tableName);

        if(empty($postData)) {
            foreach ($fields as $key => $value) {
                if(!$value['column_default'] && $value['data_type'] == 'character varying') {
                    $fname = $value['column_name'];
                    break;
                }
            }
            $query  = "INSERT INTO {$tableName} ({$fname}) VALUES('test')";
        } else {
            $fieldList = $valueList = array();
            foreach ($fields as $fname => $value) {
                $fieldType = $value['data_type'];
                $mystring  = $value['column_default'];
                $findme    = 'nextval(';
                $pos = strpos($mystring, $findme);
                if ($pos === false) {
                    if(!empty($postData[$fname])) {
                        $fieldList[] = $fname;
                        if($fieldType == 'integer') {
                            $valueList[] = $postData[$fname];
                        } else {
                            $valueList[] = "'" . $postData[$fname] . "'";
                        }
                    }
                }

            }

            $query  = "INSERT INTO {$tableName} 
                       (" . implode(',', $fieldList). ") 
                       VALUES(" . implode(',', $valueList). ")";
        }

        // lg($query);

        $result = $this ->exec($query);
        return $result;
    }

    // -- Получаем данные из таблицы
    protected function getTableData($tableName){
        $query = "SELECT * FROM {$tableName}";
        return $this->select($query);
    }

}
